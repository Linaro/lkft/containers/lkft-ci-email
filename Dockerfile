FROM python:3-slim-buster

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /

# hadolint ignore=DL3008
RUN apt-get update && apt-get install --no-install-recommends --quiet --yes \
        curl \
        git \
        jq \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

COPY LICENSE README.md requirements.txt /

RUN pip install --no-cache-dir --quiet -r requirements.txt

COPY build-email send-email /usr/local/bin/
